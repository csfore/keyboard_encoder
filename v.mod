Module {
	name: 'keyboard_encoder'
	description: 'Program meant to convert qwerty input to dvorak output and vice versa'
	version: '0.0.1'
	license: 'GPLv3'
	dependencies: []
}
