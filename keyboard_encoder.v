module main
import os
import flag

fn main() {
	mut fp := flag.new_flag_parser(os.args)
    fp.application('kenc')
    fp.version('v0.0.1')
    //fp.limit_free_args(0, 0)? // comment this, if you expect arbitrary texts after the options
    fp.description('A tool to convert qwerty keyboard input to what it would look like on a dvorak keyboard')
    fp.skip_executable()
    qwerty_flag := fp.bool('qwerty', `q`, false, 'Sets to output qwerty')
	dvorak_flag := fp.bool('dvorak', `d`, false, 'Sets to output dvorak')
    additional_args := fp.finalize() or {
        eprintln(err)
        println(fp.usage())
        return
    }
    // println('qwerty: $qwerty_flag | dvorak: $dvorak_flag')
    // println(additional_args.join(' '))
	input := additional_args.join(' ')

	if dvorak_flag {
		to_dvorak(input)
	} else if qwerty_flag {
		to_qwerty(input)
	}
}

fn to_dvorak(input string) {
	mut result_arr := []string{}
	// input := os.input("Input: ")
	for item in input {
		for key, value in qwerty_to_dvorak {
			if key == item.ascii_str() {
				result_arr << value
			}
		}
	}
	// println(result_arr)
	result := result_arr.join('')
	println(result)
}

fn to_qwerty(input string) {
	mut result_arr := []string{}
	for item in input {
		for key, value in qwerty_to_dvorak {
			if value == item.ascii_str() {
				result_arr << key
			}
		}
	}
	resultq := result_arr.join('')
	println(resultq)
}